# Create headings on your first webpage

You are ready to start writing HTML! Let's start with heading tags. Use the editor below the video to follow along with me. Pause and rewind the video as needed.

<!-- blank line -->
<figure class="video_container">
  <iframe style="width: 100%; min-height: 32vw; border: solid 1px #DDD; margin: 0;"  src="https://player.vimeo.com/video/321911629" frameborder="0" allowfullscreen="true"> </iframe>
  <iframe style="width: 100%;" title="Beasley the Bard starter" src="//codepen.io/dstrus/embed/QYZzqR/?height=460&amp;theme-id=0&amp;default-tab=html,result&amp;editable=true" height="460" allowfullscreen="allowfullscreen">
  See the Pen <a href="https://codepen.io/dstrus/pen/QYZzqR/">Beasley the Bard starter</a> by Davey Strus
  (<a href="https://codepen.io/dstrus">@dstrus</a>) on <a href="https://codepen.io">CodePen</a>.</iframe>
</figure>
<!-- blank line -->

<ul class="collapsible">

<li>

<div class="collapsible-header">Video Transcript</div>

<div class="collapsible-body">

Text

</div>

</li>

</ul>

Clicking ".5x" in the editor might make the output window look nicer for you.
